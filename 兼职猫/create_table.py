import DBUtil


def job():
    sql = '''CREATE TABLE `job` (
      `visitors` int(10) DEFAULT NULL,
      `time` varchar(50) DEFAULT NULL,
      `area_name` varchar(10) DEFAULT NULL,
      `url` varchar(100) DEFAULT NULL,
      `job_name` varchar(255) DEFAULT NULL,
      `quantity` int(10) DEFAULT NULL,
      `job_add` varchar(800) DEFAULT NULL,
      `time_request` varchar(50) DEFAULT NULL,
      `job_type` varchar(255) DEFAULT NULL,
      `Notification_time` varchar(255) DEFAULT NULL,
      `salary` decimal(10,0) DEFAULT NULL,
      `job_time` varchar(100) DEFAULT NULL,
      `job_details` varchar(1000) DEFAULT NULL,
      `company_name` varchar(50) DEFAULT NULL,
      `company_introduction` varchar(1000) DEFAULT NULL,
      `company_add` varchar(255) DEFAULT NULL,
      `Status_code` int(2) DEFAULT NULL
    )ENGINE=InnoDB DEFAULT CHARSET=utf8;'''
    DBUtil.modifyTable(sql)


def city():
    sql='''
    CREATE TABLE `city` (
  `id` varchar(200) NOT NULL,
  `city_name` varchar(20) DEFAULT NULL,
  `city_url` varchar(100) DEFAULT NULL,
  `status` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
  )ENGINE=InnoDB DEFAULT CHARSET=utf8;'''
    DBUtil.modifyTable(sql)
def area():
    sql='''
    CREATE TABLE `area` (
  `id` varchar(50) NOT NULL,
  `area_name` varchar(10) DEFAULT NULL,
  `area_url` varchar(100) DEFAULT NULL,
  `city_id` varchar(50) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `read_or_not` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    '''
    DBUtil.modifyTable(sql)
