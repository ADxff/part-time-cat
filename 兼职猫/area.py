import requests
from lxml import etree
import DBUtil
import Sha1Util

headers = {
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
                  "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
}
def get_text(url):
    """
    发送请求
    :param url:
    :return:
    """

    respon = requests.get(url=url, headers=headers)
    respon.encoding='utf-8'
    resp = respon.text
    return resp, respon.status_code

def get_city_url():
    sql = "select * from city where status != 500"
    result = DBUtil.query(sql)
    # print(result)
    return result


# get_city_url()
def area_data():
    result = get_city_url()
    for el in result:
        url = el[2]
        page_text = get_text(url)[0]
        tree = etree.HTML(page_text)[0]
        area_name_list = tree.xpath('//ul[@class="box"]/li[3]/a[@class="tab"]/text()')
        # print(area_name_list)
        area_url_list = tree.xpath('//ul[@class="box"]/li[3]/a[@class="tab"]/@href')
        # print(area_url_list)
        area_data_dict = dict(zip(area_name_list, area_url_list))
        # print(area_data_dict)
        for name, href in area_data_dict.items():
            ide = Sha1Util.jia_mi(href + name)
            href = url + href
            # print(name)
            # print(href)
            page_text = get_text(href)[0]
            tree = etree.HTML(page_text)
            page = tree.xpath('//*[@id="content_list_wrap"]/li[1]/text()')  # 获取页面是否有数据
            pagelen  = len(page)  # pagelen=9时可以访问且有数据，pagelen=2时可以访问但没有有数据，pagelen=0时无法访问
            #print(pagelen)
            a = -1  # 状态码
            sql = '''
            insert ignore into area(id,area_name,area_url,city_id,status,read_or_not) 
            values
            ("%s", "%s", "%s","%s","%s", "%s")
            ''' % (ide, name, href, el[0], pagelen, a)
            DBUtil.modifyTable(sql)
            print('爬取成功')


area_data()
