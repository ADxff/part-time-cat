from hashlib import sha1
from time import localtime
from datetime import date, timedelta
import time


def get_date(str):
    str_list = ['刚刚', '分钟', '小时']
    if str_list[0] in str or str_list[1] in str or str_list[2] in str:
        current = time.strftime("%Y-%m-%d", time.localtime())
        return current
    elif str == '昨天':
        yesterday = (date.today() + timedelta(days=-1)).strftime("%Y-%m-%d")
        return yesterday
    elif str == '前天':
        before = (date.today() + timedelta(days=-2)).strftime("%Y-%m-%d")
        return before
    else:
        return str


def jia_mi(s):
    sh = sha1()
    sh.update(s.encode())
    return sh.hexdigest()

# def check_url_area():
#    sql = 'select url,cityId from quyu'
#    result = DBUtil.query(sql)
#    for row in result:
#        url = row[0]
#        cityId = row[1]
#        re = GetData.get_text(url)[1]
#        if re == 404:
#            sql = 'update area set status = -1 where cityId = "%s"'%cityId
#            DBUtil.modify(sql)
#            print(url)