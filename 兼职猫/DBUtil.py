import pymysql
import  threading
conn = pymysql.connect(host='localhost',user='root',password='123456',database='py_sql_test')
cursor = conn.cursor()
lock = threading.RLock() # 获取锁

def createTable(sql):
    '''
    创建表结构
    :param sql:
    :return:
    '''
    cursor.execute(sql)

def modifyTable(sql):
    '''
    insert update delete
    :param sql:
    :return:
    '''
    try:
        lock.acquire() # 加锁
        cursor.execute(sql)
        conn.commit()
        lock.release() # 解锁
    except Exception as e:
        print(e)
        conn.rollback()

def query(sql):
    '''
    查询
    :param sql:
    :return:
    '''
    cursor.execute(sql)
    result = cursor.fetchall()
    return result