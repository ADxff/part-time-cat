import requests
from lxml import etree
import DBUtil
import time
from concurrent.futures import ThreadPoolExecutor

header = {
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"
                  "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36"
}


def get_text(url):
    respon = requests.get(url=url, headers=header)
    resp = respon.text
    return resp, respon.status_code


def get_area_url():
    sql = 'select * from job where Status_code != 1'
    result = DBUtil.query(sql)
    return result


def get_job_details(el):
    url = el[3]  # 获取区域链接
    resp = get_text(url)[0]
    time.sleep(0.01)
    tree = etree.HTML(resp)[0]
    work_detail = tree.xpath('//div[@id="job_detail"]/text()')  # 工作详情
    work_detail = [value.replace('"', '') for value in work_detail]  # 去列表中的除双引号
    company_name = tree.xpath('//div[@class="company_info"]/a/text()')  # 公司名
    if company_name != 0:
        company_name == company_name
    else:
        company_name = ["-1"]
    company_name = [value.replace('"', '') for value in company_name]  # 去列表中的除双引号

    company_introduction = tree.xpath('//div[@class="company_info"]/p[1]/text()')  # 公司介绍
    company_introduction = [value.replace('"', '') for value in company_introduction]  # 去列表中的除双引号
    if len(company_introduction) != 0:
        company_introduction == company_introduction
    else:
        company_introduction = ["-1"]
    # print(company_introduction)
    company_add = tree.xpath('//div[@class="company_info"]/p[2]/text()')  # 公司地址
    company_add = [value.replace('"', '') for value in company_add]  # 去列表中的除双引号
    if len(company_add) != 0:
        company_add == company_add
    else:
        company_add = ["-1"]
    # print(company_add)
    salary = tree.xpath('//div[@class="job_content"]/ul[3]/li[2]/span[2]/text()')  # 薪资
    # print(salary)
    salary_method_list = tree.xpath(
        '//div[@class="job_content"]/ul[2]/li//span[2]/text()')  # 时间要求，工作种类，工作种类，上班时段（未去除空元素工作种类）
    salary_method = [x.strip() for x in salary_method_list if x.strip() != ' ']  # 时间要求，工作种类，工作时间，上班时段（删除列表中的空元素）
    # print(salary_method)
    # 判断获取的（时间要求，工作种类，每周上班时间，上班时段。由于有些没有时间要求）进行判断长度，如果长度等于4则返回（时间要求）否则返回-1
    work_add_list = salary_method[0] if len(salary_method_list) == 4 else -1
    if len(salary_method) == 4:
        salary_method == salary_method
    else:
        salary_method.insert(0, work_add_list)  # 将时间要求等于-1的传回work_add_list列表里
    work_shijianyaoqiu = []  # 创建空时间要求
    work_zhonglie = []  # 创建空工作种类
    work_shiduan = []  # 创建空工作种类
    work_time = []  # 创建空上班时段
    work_shijianyaoqiu.append(salary_method[0])  # 时间要求加入新的列表
    work_zhonglie.append(salary_method[1])  # 工作种类加入新的列表
    work_shiduan.append(salary_method[2])  # 工作种类加入新的列表
    work_time.append(salary_method[3])  # 上班时段加入新的列表
    num = tree.xpath('//div[@class="job_content"]/ul[1]//li[1]/span[2]/text()')  # 招聘人数
    work_add = tree.xpath('//div[@class="job_content"]/ul[1]/li[2]/span[2]/text()')  # 工作地址
    # 将获取所有数据整合为一个列表
    all_list = list(
        zip(num, work_add, work_shijianyaoqiu, work_zhonglie, salary, work_shiduan, work_detail, company_name,
            company_introduction, company_add, work_time))
    #print(all_list)
    for a in all_list:
        status_code = 0  # 状态码0在写入中
        sql = 'update job set Status_code = "%d"  where url = "%s"' % (status_code, url)
        DBUtil.modifyTable(sql)
        sql1 = '''update job set quantity = "%s",job_add= "%s",time_request= "%s",job_type= "%s",
                  Notification_time= "%s",salary= "%s",job_time="%s",job_details= "%s",company_name= "%s",
                  company_introduction= "%s",company_add= "%s" 
                  where url = "%s"
        ''' % (a[0], a[1], a[2], a[3], a[10], a[4], a[5], a[6], a[7], a[8], a[9], url)
        DBUtil.modifyTable(sql1)
        status_code = 1
        sql2 = 'update job set Status_code = "%d"  where url = "%s"' % (status_code, url)
        DBUtil.modifyTable(sql2)
        print("入库成功")


def my_thread():
    result = get_area_url()
    time.sleep(0.2)
    with ThreadPoolExecutor(16) as th:
        for el in result:
            th.submit(get_job_details, el)


my_thread()
