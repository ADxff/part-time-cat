import requests
from lxml import etree
import DBUtil
import Sha1Util
url = "https://www.jianzhimao.com/ctrlcity/changeCity.html"
headers = {
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
                      "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
    }
def get_text(url):
    respon = requests.get(url=url, headers=headers)
    respon.encoding='utf-8'
    resp = respon.text
    return resp, respon.status_code
def get_city_data():
    resp = get_text(url)[0]
    tree = etree.HTML(resp)
    city_url_list = tree.xpath('//ul[@class="city_table"]/li/a/@href')
    # print(city_url_list)
    city_data_list = tree.xpath('//ul[@class="city_table"]/li/a/text()')
    # print(city_data_list)
    city_data_dict = dict(zip(city_data_list, city_url_list))
    #print(city_data_dict)
    for key, val in city_data_dict.items():
        ide = Sha1Util.jia_mi(key + val)

        sql = "insert ignore into city(id,city_name,city_url) values ('%s','%s','%s')" % (ide, key, val)
        DBUtil.modifyTable(sql)
    sql = "select * from city"  # 查询status不等于500（剔除出不能访问的城市），城市表
    result = DBUtil.query(sql)
    # print(result)
    for el in result:
        url2 = el[2]
        respon = requests.get(url=url2, headers=headers)
        respon.encoding = 'utf-8'
        status=respon.status_code

        #print(status)# 网页能成功访问返回200，如果无法访问返回500
        sql = 'update city set status = "%d" where city_url = "%s"' % (status, url2)  # 获取的status加入数据库
        DBUtil.modifyTable(sql)
get_city_data()

