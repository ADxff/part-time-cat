from hashlib import sha1

def jia_mi(s):
    '''
    id 加密
    :param s:
    :return:
    '''
    sh = sha1()
    sh.update(s.encode())
    return sh.hexdigest()