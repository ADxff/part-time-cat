import requests
from lxml import etree
import DBUtil
import time
import Time_conversion
from concurrent.futures import ThreadPoolExecutor
headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
                      "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36"
    }
def get_text(url):
    respon = requests.get(url=url, headers=headers)
    resp = respon.text
    return resp, respon.status_code
def get_area_url():#获取区域url
    sql = 'select * from area where status = 9 and read_or_not != 1'
    result = DBUtil.query(sql)
    return result
#get_area_url()
def get_job_details(el):
    area_name=el[1]
    url=el[2]
    str = url.split('//', 2)
    url_split = str[0] + '//' + str[1]
    resp = get_text(url)[0]
    tree = etree.HTML(resp)[0]
    page = tree.xpath('//ul[@class="content_page_wrap"]/li/a/text()')
    if len(page) > 1:
        page.pop()
    elif len(page) == 1:
        pass
    else:
        page==page

    for p in page:
        url_page = url + f'/index{p}.html'
        resp = get_text(url_page)[0]
        tree = etree.HTML(resp)
        name = tree.xpath('//ul[@class="content_list_wrap"]/li/a/text()')  # 工作简介
        visit = tree.xpath('//ul[@class="content_list_wrap"]//li/div[@class="left visited"]/span/@title')  # 浏览人数
        time_shijian = tree.xpath('//ul[@class="content_list_wrap"]//li/div[@class="left date"]/@title')  # 发布时间
        href_list = tree.xpath('//ul[@class="content_list_wrap"]//li/a/@href')  # url
        url_list = []
        for h in href_list:
            href = url_split + h
            url_list.append(href)
        Status_code=0
        sql = 'update area set read_or_not = "%d"  where area_url = "%s"' % (Status_code, url)
        DBUtil.modifyTable(sql)
        for p in range(len(name)):
            visit_value = visit[p]
            name_value = name[p]
            time_value = Time_conversion.get_date(time_shijian[p])  # 刚刚、分钟前、小时前、昨天、前天
            url_href = url_list[p]
            s = -1  # a=-1时详情页没有写入
            sql = '''
            insert ignore into job(visitors,time,area_name,url,job_name,Status_code) 
            values 
            ("%s","%s","%s","%s","%s","%s")
            ''' % (visit_value, time_value, area_name, url_href, name_value, s)
            DBUtil.modifyTable(sql)
    Status_code=1
    sql = 'update area set read_or_not = "%d"  where area_url = "%s"' % (Status_code, url)
    DBUtil.modifyTable(sql)
def my_thread():
    result = get_area_url()
    time.sleep(0.005)
    with ThreadPoolExecutor(10) as th:
        for el in result:
            th.submit(get_job_details(el))
my_thread()




